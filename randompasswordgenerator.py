Problem:- Generating random password by taking user input for length of password.


Solution:-

import random

length_of_password = int(input("Enter length of password to be generated: "))
alphabet = "abcdefghaiklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_-+=[{]}\|?/."
my_pass = ""

for i in range(length_of_password):
    index = random.randrange(len(alphabet))
    my_pass += alphabet[index]

print(my_pass)